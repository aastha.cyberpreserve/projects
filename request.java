/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aastha Sahni
 */
import com.google.gson.Gson;
import java.net.URL;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
public class request {
    public request() {
    }
 
    private void doRequest() {
        URL serverURL = null;
        try {
             
            test obj = new test();
            obj.setUsername("aastha");
            obj.setPassword("12345");
            obj.setLat(28.61);
            obj.setLon(77.08);
            
             
            Gson g = new Gson();
            String json;
            json = g.toJson(obj);
             
            System.out.println(json);
 
            HttpClient client = HttpClientBuilder.create().build();

             
            HttpPost httppost = new HttpPost("http://localhost:8080/webapp/sayhello");
            StringEntity stringEntity = new StringEntity(json);
            stringEntity.setContentType("application/json");
            httppost.setEntity(stringEntity);
             
            System.out.println("executing request " + httppost.getRequestLine());
            HttpResponse response = client.execute(httppost);
            HttpEntity resEntity = response.getEntity();
             
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            if (resEntity != null) {
                System.out.println("Response content length: " + resEntity.getContentLength());
                System.out.println("Chunked?: " + resEntity.isChunked());
            }
           String responseString = EntityUtils.toString(resEntity,"UTF-8");
           System.out.println(responseString);
             
        } catch (Exception e) {
            e.printStackTrace();
        }
}
public static void main(String[] args) {
        request obj1 = new request();
        obj1.doRequest();
    }

  
      
}