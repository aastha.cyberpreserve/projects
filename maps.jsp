<!DOCTYPE html>
<html>
  <head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
  </head>
  <body>
   
      <div id="map"></div>
      
       <%
          Double lati =(Double)request.getAttribute("lati");
          Double longi = (Double)request.getAttribute("longi");
         %>
         <input type="hidden" id="L" value="<%=lati%>">
         <input type="hidden" id="M" value="<%=longi%>">
   <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat:console.log(document.getElementById("L").value) , lng:console.log(document.getElementById("M").value },
          zoom: 8
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7uDo_Syz6Mbjgi5WaZLHVTEtkTbtHgcs&callback=initMap"
    async defer></script>
  </body>
</html>